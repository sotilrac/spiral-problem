""" Spiral Problem
Write a function that takes in a grid of numbers (as a list of lists), and outputs them as
a list in spiral order -- starting from the top left, in clockwise direction.
"""

grid = [
  [1, 2, 3, 0],
  [4, 3, 5, 2],
  [6, 7, 8, 3],
  [9, 0, 1, 2],
]

def sort_spiral(grid):
    spiral = list()
    
    while(grid):
        try:
            # print 'Grid: {}'.format(grid)
            spiral.append(grid.pop(0))
            for j in range(len(grid)):
                spiral.append(grid[j].pop(-1))
            last_row = list(grid.pop(-1))
            last_row.reverse()
            spiral.append(last_row)
            for j in range(len(grid)):
                spiral.append(grid[-j -1].pop(0))
        except IndexError:
            pass
    return spiral

spiral_test = sort_spiral(grid)
print spiral_test
